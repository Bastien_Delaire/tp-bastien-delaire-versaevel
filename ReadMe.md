# TP Lycée Gustave Eiffel 2020-2021

> * Auteur : Bastien delaire
> * Date : 06/04/2021
> * os: Windows 10

## Sommaire
- 1. Introduction


     - [helloWord](Introduction/HelloWord_js/index.html)
     - [Calcul Surface](Introduction/Calcul_Surface/index.html)
     -  [Calcul IMC](Introduction/Calcul_Imc/index.html)
     -  [Conversion celcius/fahrenheint](Introduction/conversion_celcius/index.html)
     -  [interpretation imc](structure_de_contrôle/activité_1/index.html)
     -  [Suite de Syracuse](structure_de_contrôle/activité2/index.html)
     -  [factoriel](structure_de_contrôle/exo1_calul_factoriel/index.html)
     -  [euros/dollars](structure_de_contrôle/exercice2_conversion_monnai/index.html)
     -  [Nombre triple](structure_de_contrôle/exercice3_nbTriple/index.html)
     -  [activité 1 fonction ](fonction/activité1/index.html)
     
  
